package com.example.ash.algoapp;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ash on 17/6/16.
 */
public class DataProvider extends BaseExpandableListAdapter {
    private Context ctx;
    private HashMap<String,List<String>>ALgo_category;
    private List<String >Algo_list;

    public DataProvider(Context ctx,HashMap<String,List<String>> ALgo_category,List<String >Algo_list)
    {
        this.ctx=ctx;
        this.ALgo_category=ALgo_category;
        this.Algo_list=  Algo_list;

    }
    @Override
    public int getGroupCount() {
        return Algo_list.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return ALgo_category.get(Algo_list.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return Algo_list.get(i);
    }

    @Override
    public Object getChild(int parent, int child) {
        return ALgo_category.get(Algo_list.get(parent)).get(child);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


    @Override
    public View getGroupView(int i, boolean b, View Convertview, ViewGroup ParentView) {
        String Title;
        Title=(String) getGroup(i);
        if(Convertview==null)
        {
            LayoutInflater inflater=(LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Convertview=inflater.inflate(R.layout.parent_layout,null);
        }
        TextView parent_view=(TextView) Convertview.findViewById(R.id.textView5);

        parent_view.setText(Title);
        return Convertview;
    }

    @Override
    public View getChildView(int parent, int child, boolean lastChild, View ConvertView , ViewGroup viewGroup) {
     String Title=(String) getChild(parent,child);
        if(ConvertView==null)
        {
            LayoutInflater inflater =(LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ConvertView=inflater.inflate(R.layout.child_layout,null);

        }
        TextView chid_text_view=(TextView) ConvertView.findViewById(R.id.textView8);
        chid_text_view.setText(Title);

        return ConvertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }



}
