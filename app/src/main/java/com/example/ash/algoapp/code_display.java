package com.example.ash.algoapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class code_display extends AppCompatActivity implements View.OnTouchListener {
    String FILE_RECEIVE,FOLDER_RECEIEVE,PATH;
    ImageButton img;
    ZoomControls Zoom;
    Intent intent_activity;
    TextView main_code;
    TextView title;



    //===================



    //==============





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_display);
        intent_activity=getIntent();
        main_code=(TextView)findViewById(R.id.textView);
        Zoom=(ZoomControls) findViewById(R.id.zoomControls);
        SetZoomControls();
        setTExtViewPinch();



        FILE_RECEIVE=intent_activity.getExtras().getString("FILE_NAME","NOT_FOUND");
        FOLDER_RECEIEVE=intent_activity.getExtras().getString("FOLDER_NAME","NOT_FOUND");
        PATH=intent_activity.getExtras().getString("COMPLETE_PATH","NOT_FOUND");


        title=(TextView)findViewById(R.id.textView4);
        title.setText(FILE_RECEIVE);
        SharedPreferences COLOR=getSharedPreferences("COLORSSS", Context.MODE_PRIVATE);






        main_code.setTextColor(COLOR.getInt("Text",0));
        main_code.setBackgroundColor(COLOR.getInt("Background",0));
        set_content();

        img=(ImageButton)findViewById(R.id.imageButton5);
        img.setImageResource(R.drawable.star_filled);

        SharedPreferences my_personal_list=getSharedPreferences("BOOKMARKS",Context.MODE_PRIVATE);
        if(my_personal_list.contains(FILE_RECEIVE))
            {
                img.setImageResource(R.drawable.star_filled);
            }
        else
            {
               img.setImageResource(R.drawable.add_bookmark_logo);
            }


    }
    public void set_content()
    {
        InputStream ss;
        String code;
        String content = "";
        try{

            AssetManager assetManager = getAssets();
           String[] files = assetManager.list("Codes");
            ss=getAssets().open("Codes/"+PATH);

            content= convert_to_string(ss);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

     main_code.setText(content);

      //  main_code.setMovementMethod(new ScrollingMovementMethod());



    }
    public  String convert_to_string(InputStream ss)
    {

        BufferedReader bufferedReader=null;
        StringBuilder stringBuilder=new StringBuilder();
        String line;
        try{
            bufferedReader= new BufferedReader(new InputStreamReader(ss));
            while ((line=bufferedReader.readLine())!=null)
            {    stringBuilder.append("\n");
                stringBuilder.append(line);
            }
        }catch (IOException e){
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return  stringBuilder.toString();
    }//end convert_to string

    public void bookmark_button(View view)
    {



        SharedPreferences my_preference=getSharedPreferences("BOOKMARKS", Context.MODE_PRIVATE);
        SharedPreferences my_preference_count=getSharedPreferences("Count", Context.MODE_PRIVATE);
        SharedPreferences.Editor my_preference_editor=my_preference.edit();
        SharedPreferences.Editor my_preference_editor_count=my_preference_count.edit();


        if(my_preference_count.contains("Count")==false)
            {
                my_preference_editor_count.putInt("Count",0);
                my_preference_editor_count.commit();
            }

        if(my_preference.contains(FILE_RECEIVE))
            {
                int count=my_preference_count.getInt("Count",-1);
                count--;
                my_preference_editor_count.putInt("Count",count);
                my_preference_editor.remove(FILE_RECEIVE);
                my_preference_editor.commit();
                my_preference_editor_count.commit();
                Toast.makeText(getApplicationContext(),"Removed from personal list",Toast.LENGTH_SHORT).show();
                img.setImageResource(R.drawable.add_bookmark_logo);
            }

        else
            {

                int count=my_preference_count.getInt("Count",-1);
                count++;
                my_preference_editor_count.putInt("Count",count);
                my_preference_editor.putString(FILE_RECEIVE,FOLDER_RECEIEVE+"/"+FILE_RECEIVE);
                my_preference_editor.commit();
                my_preference_editor_count.commit();
                Toast.makeText(getApplicationContext(),"Added to personal list",Toast.LENGTH_SHORT).show();
                img.setImageResource(R.drawable.star_filled);


            }
    }//end

    public void SetZoomControls()
    {

        //main_code refers to a TextView
        Zoom.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                main_code.setTextSize(TypedValue.COMPLEX_UNIT_PX  ,main_code.getTextSize()+1);
            }
        });

        Zoom.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                main_code.setTextSize(TypedValue.COMPLEX_UNIT_PX  ,main_code.getTextSize()-1);
    
                            }
        });



    }



    //==================================================
    public boolean eeuchEvent(MotionEvent event) {

        return true;
    }

    int getDistance(MotionEvent event) {
        int dx = (int) (event.getX(0) - event.getX(1));
        int dy = (int) (event.getY(0) - event.getY(1));
        return (int) (Math.sqrt(dx * dx + dy * dy));
    }

    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }



    //===================================================


    public void setTExtViewPinch()
    {

        final  float STEP = 100;

        final float[] mRatio = {1.0f};
        final int[] mBaseDist = new int[1];
        final float[] mBaseRatio = new float[1];

        main_code.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getPointerCount() == 2) {
                    int action = event.getAction();
                    int pureaction = action & MotionEvent.ACTION_MASK;
                    if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                        mBaseDist[0] = getDistance(event);
                        mBaseRatio[0] = mRatio[0];
                    } else {
                        float delta = (getDistance(event) - mBaseDist[0]) / STEP;
                        float multi = (float) Math.pow(2, delta);
                        mRatio[0] = Math.min(1024.0f, Math.max(0.1f, mBaseRatio[0] * multi));
                        main_code.setTextSize(mRatio[0] + 13);
                    }
                }
                return true;
            }
        });


    }







}//end cals
