package com.example.ash.algoapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  this.requestWindowFeature(Window.FEATURE_NO_TITLE);

       this.setContentView(R.layout.activity_main);
        View view = findViewById(android.R.id.content);

        Animation mLoadAnimation = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in);

        mLoadAnimation.setDuration(3000);
        view.startAnimation(mLoadAnimation);

    }

    public void showlist(View view) {

        Intent show_list_Activity=new Intent(getApplicationContext(),listdisplay.class);
        show_list_Activity.putExtra("show_whole_list",true);
        startActivity(show_list_Activity);

    }

    public void showsettings(View view)
    {

        startActivity(new Intent(getApplicationContext(),settings.class));
    }
    public void showmoreinfo(View view)
    {
        startActivity(new Intent(getApplicationContext(),moreinfo.class));
    }

    public  void showpersonallist(View view)
    {Intent show_personal_list_Activity=new Intent(getApplicationContext(),listdisplay.class);
        show_personal_list_Activity.putExtra("show_whole_list",false);
        startActivity(show_personal_list_Activity);


    }

}