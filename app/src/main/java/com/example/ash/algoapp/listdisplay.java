package com.example.ash.algoapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class listdisplay extends AppCompatActivity {
    int type=0;
    HashMap<String,List<String>> Parent_Child;
    List<String> Parent_Titles;

    Intent type_of_display;
    ListView file_listview;
    ExpandableListView folder_listview;
    AssetManager file_assetmanager;
    ProgressBar list_populate_progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdisplay);
        type_of_display=getIntent();
        file_listview=(ListView)findViewById(R.id.listView);
        file_assetmanager=getAssets();

      // HashMapGenerator mapped=new HashMapGenerator(getApplicationContext());
        if(true == type_of_display.getExtras().getBoolean("show_whole_list"))
        {
            ExpandableList();
         //   list_populate_progress.setVisibility(View.GONE);
            type=1;
        }
        else
        {
           populate_boookmarked_list();
            type=2;
       }


    }//end of on create



    public void populate_boookmarked_list()
    {

       SharedPreferences bookmarkPreference;
       bookmarkPreference = getSharedPreferences("BOOKMARKS", Context.MODE_PRIVATE);

        SharedPreferences bookmarkPreference_count;
       bookmarkPreference_count = getSharedPreferences("Count", Context.MODE_PRIVATE);

       int count=  bookmarkPreference_count.getInt("Count", -1);

        ArrayList<String> bookmark_list=new ArrayList<String >();

        final Map<String, ?> allEntries = bookmarkPreference.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {

                bookmark_list.add(entry.getKey());
        }

        final ListView bookmarkList;
        bookmarkList=(ListView)findViewById(R.id.listView);

        ArrayAdapter<String> bookmarkadapter_1=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,bookmark_list);
        bookmarkList.setAdapter(bookmarkadapter_1);
        bookmarkList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intnet=new Intent(getApplicationContext(),code_display.class);
                intnet.putExtra("FILE_NAME",(String) bookmarkList.getItemAtPosition(i));
                intnet.putExtra("FOLDER_NAME",(String)allEntries.get( bookmarkList.getItemAtPosition(i)));
                intnet.putExtra("COMPLETE_PATH",(String)allEntries.get( bookmarkList.getItemAtPosition(i)));
                startActivity(intnet);
            }
        });
    }
    public void ExpandableList()
    {
        Parent_Child=setHashMap();
        folder_listview=(ExpandableListView)findViewById(R.id.expandableListView);
        Parent_Titles=new ArrayList(Parent_Child.keySet());
        DataProvider adapter;
        adapter=new DataProvider(this,Parent_Child,Parent_Titles);
        folder_listview.setAdapter(adapter);

        folder_listview.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {

                Intent intent=new Intent(getApplicationContext(),code_display.class);

                intent.putExtra("FOLDER_NAME",Parent_Titles.get(i));
                intent.putExtra("FILE_NAME",Parent_Child.get(Parent_Titles.get(i)).get(i1));
                intent.putExtra("COMPLETE_PATH",Parent_Titles.get(i)+"/"+Parent_Child.get(Parent_Titles.get(i)).get(i1));

                startActivity(intent);
                return  false;
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();

        if(type==1)
                ExpandableList();
        else  populate_boookmarked_list();

    }

    private List<String> ConvertToList(String[] strArr)
    { List<String> stringList = new ArrayList<String>();
        for (int i=0;i<strArr.length;i++) {
            stringList.add(strArr[i]);
        }

        return stringList;

    }


    private   HashMap<String,List<String>> setHashMap()
    {
        String[] Str_codes;
        List<String> CODES;
        List<String> CODES1=new ArrayList<>();
        HashMap<String,List<String>>Hashing;
        Hashing= new HashMap<>();

        try {
            Str_codes=file_assetmanager.list("Codes");

        for(int i=0;i<Str_codes.length;i++)
            {

                List<String> Child_list=new ArrayList<>();
                String[] Str_sub_codes;
                Str_sub_codes=file_assetmanager.list("Codes"+"/"+Str_codes[i]);
                Child_list=ConvertToList(Str_sub_codes);
                Hashing.put(Str_codes[i],Child_list);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return Hashing;


    }

}//end of class
