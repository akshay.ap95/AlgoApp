package com.example.ash.algoapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

public class settings extends AppCompatActivity {
    TextView text;
    TextView back_c;
    SharedPreferences COLORsss;
    SharedPreferences.Editor addCOlour;
    int colorB;
    int colorT;
    SeekBar tuner1;
    SeekBar tuner2;
    SeekBar tuner3;
    SeekBar tuner11;
    SeekBar tuner22;
    SeekBar tuner33;
    RadioGroup option;
    Button choose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        final int[] X = new int[1];


        tuner1 = (SeekBar) findViewById(R.id.seekBar);
        tuner2 = (SeekBar) findViewById(R.id.seekBar2);
        tuner3 = (SeekBar) findViewById(R.id.seekBar3);
        tuner11 = (SeekBar) findViewById(R.id.seekBar7);
        tuner22 = (SeekBar) findViewById(R.id.seekBar8);
        tuner33 = (SeekBar) findViewById(R.id.seekBar9);


        COLORsss =getSharedPreferences("COLORSSS", Context.MODE_PRIVATE);
        addCOlour=COLORsss.edit();
        //addListener();
        text=(TextView)findViewById(R.id.textView3);
        back_c=(TextView)findViewById(R.id.textView7);

        text.setBackgroundColor(COLORsss.getInt("Text",0));
        back_c.setBackgroundColor(COLORsss.getInt("Background",0));
        SeekBar.OnSeekBarChangeListener changeListener=new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //X[0]=progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                colorT= Color.argb(255, tuner1.getProgress(), tuner2.getProgress(),tuner3.getProgress());
                colorB=Color.argb(255, tuner11.getProgress(), tuner22.getProgress(),tuner33.getProgress());
                addCOlour.putInt("Text",colorT);
                addCOlour.putInt("Background",colorB);
                addCOlour.commit();
                text.setBackgroundColor(colorT);
                back_c.setBackgroundColor(colorB);
            }
        };

        tuner1.setProgress(Color.red(COLORsss.getInt("Text", 23)));
        tuner2.setProgress(Color.green(COLORsss.getInt("Text", 23)));
        tuner3.setProgress(Color.blue(COLORsss.getInt("Text", 23)));
        tuner11.setProgress(Color.red(COLORsss.getInt("Background", 23)));
        tuner22.setProgress(Color.green(COLORsss.getInt("Background", 23)));
        tuner33.setProgress(Color.blue(COLORsss.getInt("Background", 23)));

        tuner1.setOnSeekBarChangeListener(changeListener);
        tuner2.setOnSeekBarChangeListener(changeListener);
        tuner3.setOnSeekBarChangeListener(changeListener);
        tuner11.setOnSeekBarChangeListener(changeListener);
        tuner22.setOnSeekBarChangeListener(changeListener);
        tuner33.setOnSeekBarChangeListener(changeListener);





    }




    }

